/* ------------------- Activity - Session 26 ------------------------------*/

// import http module
const http = require("http");

// create variable port and assign 3000 to it
const port = 3000;

// create a server that will listen to the port provided above
const server = http.createServer( ( request, response ) => {

	// create a condition that when the login route is accessed, it will print a message to the user that they are in the login page
	if(request.url == '/login'){
			response.writeHead(200, {'Content-Type': 'text/html'});
			response.end("Hello! Welcome to login page.");
	}

	// create a condition for any other routes that will return an error message
	else {
			response.writeHead(404, {'Content-Type': 'text/html'});
			response.end("Page is not available.");
	}

} );
server.listen(port);

// console log in the terminal a message when the server is successfully running
console.log(`Server is successfully running at localhost: ${port}.`);